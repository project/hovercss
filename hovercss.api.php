<?php

/**
 * @file
 * Hooks for the HoverCSS module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Provide hovercss effect names.
 *
 * By implementing hook_hovercss_effect_names(), a module can add
 * effect name to the list and extent css animation with new custom hover.
 *
 * @param array $effect_names
 *   List of new effect names in array.
 *
 * @return array
 *   A render array that can be added into Hover CSS effect names.
 */
function hook_hovercss_effect_names(array $effect_names = []) {
  $effect_names = [];

  return $effect_names;
}

/**
 * @} End of "addtogroup hooks".
 */
