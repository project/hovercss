INTRODUCTION
------------

This module integrates the 'Hover.css' library:

  - https://ianlunn.github.io/Hover/

Hover.css is a collection of CSS3 powered hover effects to be applied to
links, buttons, logos, SVG, featured images and so on. Easily apply to your
own elements, modify or just use for inspiration.

Available in CSS, Sass, and LESS.


FEATURES
--------

'Hover.css' library is:

  - Cross-browser animations

  - Usage with CSS

  - Easy to use

  - Responsive

  - Customizable


REQUIREMENTS
------------

'Hover.css' library:

  - https://github.com/IanLunn/Hover/archive/master.zip


INSTALLATION
------------

1. Download 'HoverCSS' module - https://www.drupal.org/project/hovercss

2. Extract and place it in the root of contributed modules directory i.e.
   /modules/contrib/hovercss

3. Create a libraries directory in the root, if not already there i.e.
   /libraries

4. Create a 'hover' directory inside it i.e.
   /libraries/hover

5. Download 'Hover.css' library
   https://github.com/IanLunn/Hover/archive/master.zip

6. Place it in the /libraries/hover directory i.e. Required files:

  - /libraries/hover/css/dist/hover.css
  - /libraries/hover/css/dist/hover-min.css

7. Now, enable 'HoverCSS' module.


USAGE
-----

Hover.css can be used in a number of ways; either copy and paste the effect
you'd like to use in your own stylesheet or reference the stylesheet.

Then just add the class name of the effect to the element you'd like it
applied to.


BASIC USAGE
===========

After installing Hover.css, add the class animate__animated to an element,
along with any of the animation names (don't forget the animate__ prefix!):

<a href="#" class="button hvr-grow">Add to Basket</a>

USAGE WITH JAVASCRIPT
=====================

You can do a bunch of other stuff with hover.css when you combine
it with Javascript. A simple example:

const element = document.querySelector('.my-element');
element.classList.add('hvr-grow');


How does it Work?
-----------------

1. Enable "HoverCSS" module, Follow INSTALLATION in above.

2. Add hover.css classes to templates or add classed with javascript file
   in your theme, Follow USAGE in above.

3. Enjoy that.

Animations can improve the UX of an interface, but keep in mind that they can
also get in the way of your users! Please read the best practices and gotchas
sections to bring your web-things to life in the best way possible.


MAINTAINERS
-----------

Current module maintainer:

 * Mahyar Sabeti - https://www.drupal.org/u/mahyarsbt


DEMO
----
https://ianlunn.github.io/Hover/
