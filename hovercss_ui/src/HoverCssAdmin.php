<?php

namespace Drupal\hovercss_ui;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\hovercss_ui\Form\HoverCssFilter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Displays Hover CSS selector.
 *
 * @internal
 */
class HoverCssAdmin extends FormBase {

  /**
   * Hover manager.
   *
   * @var \Drupal\hovercss_ui\HoverCssManagerInterface
   */
  protected $effectManager;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructs a new hover object.
   *
   * @param \Drupal\hovercss_ui\HoverCssManagerInterface $effect_manager
   *   The hover selector manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   The current request.
   */
  public function __construct(HoverCssManagerInterface $effect_manager, DateFormatterInterface $date_formatter, FormBuilderInterface $form_builder, Request $current_request) {
    $this->effectManager  = $effect_manager;
    $this->dateFormatter  = $date_formatter;
    $this->formBuilder    = $form_builder;
    $this->currentRequest = $current_request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hovercss.effect_manager'),
      $container->get('date.formatter'),
      $container->get('form_builder'),
      $container->get('request_stack')->getCurrentRequest(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hover_admin_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param int $hid
   *   (optional) Record ID of hover effects.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $hid = 0) {
    $form['#attached']['library'][] = 'hovercss_ui/hover-list';

    $search = $this->currentRequest->query->get('search');
    $status = $this->currentRequest->query->get('status') ?? NULL;

    /** @var \Drupal\hovercss_ui\Form\HoverCssFilter $form */
    $form['hovercss_admin_filter_form'] = $this->formBuilder->getForm(HoverCssFilter::class, $search, $status);
    $form['#attributes']['class'][] = 'hovercss-filter';
    $form['#attributes']['class'][] = 'views-exposed-form';

    $header = [
      [
        'data'  => $this->t('Selector'),
        'field' => 'h.hid',
      ],
      [
        'data'  => $this->t('Label'),
        'field' => 'h.label',
      ],
      [
        'data'  => $this->t('Status'),
        'field' => 'h.status',
      ],
      [
        'data'  => $this->t('Updated'),
        'field' => 'h.changed',
        'sort'  => 'desc',
      ],
      $this->t('Operations'),
    ];

    $rows = [];
    $result = $this->effectManager->findAll($header, $search, $status);
    foreach ($result as $hover) {
      $row = [];
      $row['selector'] = $hover->selector;
      $row['label'] = $hover->label;
      $status_class = $hover->status ? 'marker marker--enabled' : 'marker';
      $row['status'] = [
        'data' => [
          '#type' => 'markup',
          '#prefix' => '<span class="' . $status_class . '">',
          '#suffix' => '</span>',
          '#markup' => $hover->status ? $this->t('Enabled') : $this->t('Disabled'),
        ],
      ];
      $row['changed'] = $this->dateFormatter->format($hover->changed, 'short');
      $links = [];
      $links['edit'] = [
        'title' => $this->t('Edit'),
        'url'   => Url::fromRoute('hovercss.edit', ['hid' => $hover->hid]),
      ];
      $links['delete'] = [
        'title' => $this->t('Delete'),
        'url'   => Url::fromRoute('hovercss.delete', ['hid' => $hover->hid]),
      ];
      $links['duplicate'] = [
        'title' => $this->t('Duplicate'),
        'url'   => Url::fromRoute('hovercss.duplicate', ['hid' => $hover->hid]),
      ];
      $row[] = [
        'data' => [
          '#type'  => 'operations',
          '#links' => $links,
        ],
      ];
      $rows[] = $row;
    }

    $form['hovercss_admin_table'] = [
      '#type'   => 'table',
      '#header' => $header,
      '#rows'   => $rows,
      '#empty'  => $this->t('No hover CSS selector available. <a href=":link">Add effect</a> .', [
        ':link' => Url::fromRoute('hovercss.add')
          ->toString(),
      ]),
      '#attributes' => ['class' => ['hovercss-list']],
    ];

    $form['pager'] = ['#type' => 'pager'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Add operations to hover CSS selector list
  }

}
