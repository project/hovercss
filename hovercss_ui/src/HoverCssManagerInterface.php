<?php

namespace Drupal\hovercss_ui;

/**
 * Provides an interface defining an effect manager.
 */
interface HoverCssManagerInterface {

  /**
   * Returns if this Hover css selector is added.
   *
   * @param string $hover
   *   The Hover css selector to check.
   *
   * @return bool
   *   TRUE if the Hover css selector is added, FALSE otherwise.
   */
  public function isHover($hover);

  /**
   * Finds an added hover css selector by its ID.
   *
   * @return string|false
   *   Either the added hover selector or FALSE if none exist with that ID.
   */
  public function loadHover();

  /**
   * Add a hover css selector.
   *
   * @param int $hover_id
   *   The hover id for edit.
   * @param string $selector
   *   The hover selector to add.
   * @param string $label
   *   The label of hover selector.
   * @param string $comment
   *   The comment for hover options.
   * @param int $changed
   *   The expected modification time.
   * @param int $status
   *   The status for hover.
   * @param string $options
   *   The hover selector options.
   *
   * @return int|null|string
   *   The last insert ID of the query, if one exists.
   */
  public function addHover($hover_id, $selector, $label, $comment, $changed, $status, $options);

  /**
   * Remove a hover css selector.
   *
   * @param int $hover_id
   *   The hover id to remove.
   */
  public function removeHover($hover_id);

  /**
   * Finds all added Hover css selector.
   *
   * @param array $header
   *   The hover header to sort selector and label.
   * @param string $search
   *   The hover search key to filter selector.
   * @param int|null $status
   *   The hover status to filter selector.
   *
   * @return \Drupal\Core\Database\StatementInterface
   *   The result of the database query.
   */
  public function findAll(array $header, $search, $status);

  /**
   * Finds an added Hover css selector by its ID.
   *
   * @param int $hover_id
   *   The ID for an added hover selector.
   *
   * @return string|false
   *   Either the added hover selector or FALSE if none exist with that ID.
   */
  public function findById($hover_id);

}
