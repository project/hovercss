<?php

namespace Drupal\hovercss_ui\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hovercss_ui\HoverCssManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to duplicate hover.
 *
 * @internal
 */
class HoverCssDuplicate extends FormBase {

  /**
   * The hover id.
   *
   * @var int
   */
  protected $hover;

  /**
   * The hover selector manager.
   *
   * @var \Drupal\hovercss_ui\HoverCssManagerInterface
   */
  protected $effectManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new hoverDuplicate object.
   *
   * @param \Drupal\hovercss_ui\HoverCssManagerInterface $effect_manager
   *   The hover selector manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(HoverCssManagerInterface $effect_manager, TimeInterface $time) {
    $this->effectManager = $effect_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hovercss.effect_manager'),
      $container->get('datetime.time'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hovercss_duplicate_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param int $hid
   *   The hover record ID to remove.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $hid = 0) {
    $form['hover_id'] = [
      '#type'  => 'value',
      '#value' => $hid,
    ];

    // New selector to duplicate effect.
    $form['selector'] = [
      '#title'         => $this->t('Selector'),
      '#type'          => 'textfield',
      '#required'      => TRUE,
      '#size'          => 64,
      '#maxlength'     => 255,
      '#default_value' => '',
      '#description'   => $this->t('Here, you can use HTML tag, class with dot(.) and ID with hash(#) prefix. Be sure your selector has plain text content. e.g. ".page-title" or ".block-title".'),
      '#placeholder'   => $this->t('Enter valid selector'),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#button_type' => 'primary',
      '#value'       => $this->t('Duplicate'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $hover_id = $form_state->getValue('hover_id');
    $is_new   = $hover_id == 0;
    $selector = trim($form_state->getValue('selector'));

    if ($is_new) {
      if ($this->effectManager->isHover($selector)) {
        $form_state->setErrorByName('selector', $this->t('This selector is already exists.'));
      }
    }
    else {
      if ($this->effectManager->findById($hover_id)) {
        $hover = $this->effectManager->findById($hover_id);

        if ($selector != $hover['selector'] && $this->effectManager->isHover($selector)) {
          $form_state->setErrorByName('selector', $this->t('This selector is already added.'));
        }
      }
    }
  }

  /**
   * Form submission handler for the 'duplicate' action.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   A reference to a keyed array containing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $hover_id = $values['hover_id'];
    $selector = trim($values['selector']);
    $label    = ucfirst(trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $selector)));
    $status   = 1;

    $hover   = $this->effectManager->findById($hover_id);
    $comment = $hover['comment'];
    $options = $hover['options'];

    // The Unix timestamp when the hover was most recently saved.
    $changed = $this->time->getCurrentTime();

    // Save hover.
    $new_hid = $this->effectManager->addHover(0, $selector, $label, $comment, $changed, $status, $options);
    $this->messenger()
      ->addStatus($this->t('The selector %selector has been duplicated.', ['%selector' => $selector]));

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    // Redirect to duplicated hover edit form.
    $form_state->setRedirect('hovercss.edit', ['hid' => $new_hid]);
  }

}
