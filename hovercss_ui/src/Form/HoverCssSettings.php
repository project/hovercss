<?php

namespace Drupal\hovercss_ui\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures hover settings.
 */
class HoverCssSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hovercss_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // HoverCSS flags for specific form labels and suffix.
    $ms_unit_label = ' <span class="hovercss-unit-flag">ms</span>';

    // Get current settings.
    $config = $this->config('hovercss.settings');

    $form['settings'] = [
      '#type'  => 'details',
      '#title' => $this->t('Hover settings'),
      '#open'  => TRUE,
    ];

    // Let module handle load Hover.css library.
    $form['settings']['load'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Load Hover.css library'),
      '#default_value' => $config->get('load'),
      '#description'   => $this->t("If enabled, this module will attempt to load the Hover.css library for your site. To prevent loading twice, leave this option disabled if you're including the assets manually or through another module or theme."),
    ];

    // Let module handle hover button patch library.
    $form['settings']['patch'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Use Hover.css patch'),
      '#default_value' => $config->get('patch'),
      '#description'   => $this->t("This module will attempt to load the hovercss.patch.css to resolve Drupal button css conflicts."),
    ];

    // Show warning missing library and lock on cdn method.
    $method = $config->get('method');
    $method_lock_change = FALSE;
    if (!hovercss_check_installed()) {
      $method = 'cdn';
      $method_lock_change = TRUE;
      $method_warning = $this->t('You cannot set local due to the Hover.css library is missing. Please <a href=":downloadUrl" rel="external" target="_blank">Download the library</a> and and extract to "/libraries/hover" directory.', [
        ':downloadUrl' => 'https://github.com/IanLunn/Hover/archive/master.zip',
      ]);

      // Display warning message off.
      $form['settings']['hide'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->t('Hide warning'),
        '#default_value' => $config->get('hide') ?? FALSE,
        '#description'   => $this->t("If you want to use the CDN without installing the local library, you can turn off the warning."),
      ];

      $form['settings']['method_warning'] = [
        '#type'   => 'item',
        '#markup' => '<div class="library-status-report">' . $method_warning . '</div>',
        '#states' => [
          'visible' => [
            ':input[name="load"]' => ['checked' => TRUE],
          ],
          'invisible' => [
            ':input[name="hide"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }

    // Load method library from CDN or Locally.
    $form['settings']['method'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Add Hover.css method'),
      '#options'       => [
        'local' => $this->t('Local'),
        'cdn'   => $this->t('CDN'),
      ],
      '#default_value' => $method,
      '#description'   => $this->t('These settings control how the Hover.css library is loaded. You can choose to load from the CDN (External source) or from the local (Internal library).'),
      '#disabled'      => $method_lock_change,
    ];

    // Production or minimized version.
    $form['settings']['minimized'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Development or Production version'),
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
    ];
    $form['settings']['minimized']['minimized_options'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Choose minimized or non-minimized library.'),
      '#options'       => [
        0 => $this->t('Use non-minimized library (Development)'),
        1 => $this->t('Use minimized library (Production)'),
      ],
      '#default_value' => $config->get('minimized.options'),
      '#description'   => $this->t('These settings work with both local library and CDN methods, but this not applicable in the compat version because the compatible library is only available in minimized mode.'),
    ];

    // Load Hover.css library Per-path.
    $form['settings']['url'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Load on specific URLs'),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    ];
    $form['settings']['url']['url_visibility'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Load hover.css on specific pages'),
      '#options'       => [
        0 => $this->t('All pages except those listed'),
        1 => $this->t('Only the listed pages'),
      ],
      '#default_value' => $config->get('url.visibility'),
    ];
    $form['settings']['url']['url_pages'] = [
      '#type'          => 'textarea',
      '#title'         => '<span class="element-invisible">' . $this->t('Pages') . '</span>',
      '#default_value' => _hovercss_ui_array_to_string($config->get('url.pages')),
      '#description'   => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. An example path is %admin-wildcard for every user page. %front is the front page.", [
        '%admin-wildcard' => '/admin/*',
        '%front'          => '<front>',
      ]),
    ];

    // Hover.css Utilities,
    // Comes packed with a few utility classes to simplify its use.
    $form['options'] = [
      '#type'  => 'details',
      '#title' => $this->t('Hover default options'),
      '#open'  => TRUE,
    ];

    // List of selectors to individual hover Control with Hover.css.
    $form['options']['selectors'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Global selectors'),
      '#default_value' => _hovercss_ui_array_to_string($config->get('options.selector')),
      '#description'   => $this->t('Enter CSS selector (id/class) of your elements e.g., "#id" or ".classname". Use a new line for each selector.'),
      '#rows'          => 3,
    ];

    // The animation to use.
    $form['options']['effect'] = [
      '#title'         => $this->t('Effect'),
      '#type'          => 'select',
      '#options'       => hovercss_effect_options(),
      '#default_value' => $config->get('options.animation'),
      '#description'   => $this->t('Select the animation name you want to use for CSS selectors globally.'),
    ];

    // Hover.css provides the following delays.
    $form['options']['delay'] = [
      '#type'          => 'number',
      '#min'           => 0,
      '#title'         => $this->t('Delay'),
      '#default_value' => $config->get('options.delay'),
      '#field_suffix'  => $ms_unit_label,
      '#attributes'    => ['class' => ['hover-delay']],
    ];

    // Hover speed time and duration used as a prefix on CSS Variables.
    $form['options']['duration'] = [
      '#type'          => 'number',
      '#min'           => 0,
      '#title'         => $this->t('Duration'),
      '#default_value' => $config->get('options.duration'),
      '#field_suffix'  => $ms_unit_label,
      '#attributes'    => ['class' => ['hover-duration']],
    ];

    // Hover.css preview.
    $form['preview'] = [
      '#type'   => 'details',
      '#title'  => $this->t('Hover preview'),
      '#open'   => TRUE,
    ];

    // Hover.css button effect.
    $form['preview']['hover_primary'] = [
      '#value'        => $this->t('Input button'),
      '#type'         => 'button',
      '#button_type'  => 'primary',
      '#attributes'   => ['class' => ['hover__sample']],
      '#prefix' => '<div class="hover__preview">',
    ];
    $form['preview']['hover_secondary'] = [
      '#type'   => 'markup',
      '#markup' => '<a class="hover__sample button" href="#">' . $this->t('Link button') . '</a>',
    ];
    $form['preview']['hover_custom'] = [
      '#type'   => 'markup',
      '#markup' => '<a class="hover__sample custom-button" href="#">' . $this->t('Custom button') . '</a>',
    ];
    $form['preview']['hover_icon'] = [
      '#type'   => 'markup',
      '#markup' => '<a class="hover__sample custom-button icon-button" href="#"><i class="fa fa-chevron-circle-right hvr-icon"></i>' . $this->t('Icon button') . '</a>',
      '#suffix' => '</div>',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // @todo Hover field verification.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Save the updated Hover.css settings.
    $this->config('hovercss.settings')
      ->set('load', $values['load'])
      ->set('patch', $values['patch'])
      ->set('hide', isset($values['hide']) && $values['hide'] !== 0 ?? FALSE)
      ->set('method', $values['method'])
      ->set('minimized.options', $values['minimized_options'])
      ->set('url.visibility', $values['url_visibility'])
      ->set('url.pages', _hovercss_ui_string_to_array($values['url_pages']))
      ->set('options.selector', _hovercss_ui_string_to_array($values['selectors']))
      ->set('options.effect', $values['effect'])
      ->set('options.delay', $values['delay'])
      ->set('options.duration', $values['duration'])
      ->save();

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'hovercss.settings',
    ];
  }

}
