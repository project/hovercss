<?php

namespace Drupal\hovercss_ui\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\hovercss_ui\HoverCssManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a form to remove CSS selector.
 *
 * @internal
 */
class HoverCssDelete extends ConfirmFormBase {

  /**
   * The hover selector.
   *
   * @var int
   */
  protected $hover;

  /**
   * The hover selector manager.
   *
   * @var \Drupal\hovercss_ui\HoverCssManagerInterface
   */
  protected $effectManager;

  /**
   * Constructs a new hoverDelete object.
   *
   * @param \Drupal\hovercss_ui\HoverCssManagerInterface $effect_manager
   *   The hover selector manager.
   */
  public function __construct(HoverCssManagerInterface $effect_manager) {
    $this->effectManager = $effect_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hovercss.effect_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hovercss_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to remove %selector from hover selectors?', ['%selector' => $this->hover['selector']]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param int $hid
   *   The hover record ID to remove.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $hid = 0) {
    if (!$this->hover = $this->effectManager->findById($hid)) {
      throw new NotFoundHttpException();
    }
    $form['hover_id'] = [
      '#type'  => 'value',
      '#value' => $hid,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $hover_id = $form_state->getValue('hover_id');
    $this->effectManager->removeHover($hover_id);
    $this->logger('user')
      ->notice('Deleted %selector', ['%selector' => $this->hover['selector']]);
    $this->messenger()
      ->addStatus($this->t('The hover selector %selector was deleted.', ['%selector' => $this->hover['selector']]));

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('hovercss.admin');
  }

}
