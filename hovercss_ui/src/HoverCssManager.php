<?php

namespace Drupal\hovercss_ui;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Database\Query\TableSortExtender;

/**
 * Hover manager.
 */
class HoverCssManager implements HoverCssManagerInterface {

  /**
   * The database connection used to check the selector against.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a HoverCssManager object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection which will be used to check the selector
   *   against.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function isHover($hover) {
    return (bool) $this->connection->query("SELECT * FROM {hovercss} WHERE [selector] = :selector", [':selector' => $hover])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function loadHover() {
    $query = $this->connection
      ->select('hovercss', 'h')
      ->fields('h', ['hid', 'selector', 'options'])
      ->condition('status', 1);

    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function addHover($hover_id, $selector, $label, $comment, $changed, $status, $options) {
    $this->connection->merge('hovercss')
      ->key('hid', $hover_id)
      ->fields([
        'selector' => $selector,
        'label'    => $label,
        'comment'  => $comment,
        'changed'  => $changed,
        'status'   => $status,
        'options'  => $options,
      ])
      ->execute();

    return $this->connection->lastInsertId();
  }

  /**
   * {@inheritdoc}
   */
  public function removeHover($hover_id) {
    $this->connection->delete('hovercss')
      ->condition('hid', $hover_id)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function findAll($header = [], $search = '', $status = NULL) {
    $query = $this->connection
      ->select('hovercss', 'h')
      ->extend(PagerSelectExtender::class)
      ->extend(TableSortExtender::class)
      ->orderByHeader($header)
      ->limit(50)
      ->fields('h');

    if (!empty($search) && !empty(trim((string) $search)) && $search !== NULL) {
      $search = trim((string) $search);
      // Escape for LIKE matching.
      $search = $this->connection->escapeLike($search);
      // Replace wildcards with MySQL/PostgreSQL wildcards.
      $search = preg_replace('!\*+!', '%', $search);
      // Add selector and the label field columns.
      $group = $query->orConditionGroup()
        ->condition('selector', '%' . $search . '%', 'LIKE')
        ->condition('label', '%' . $search . '%', 'LIKE');
      // Run the query to find matching targets.
      $query->condition($group);
    }

    // Check if status is set.
    if (!is_null($status) && $status != '') {
      $query->condition('status', $status);
    }

    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function findById($hover_id) {
    return $this->connection->query("SELECT [selector], [label], [comment], [status], [options] FROM {hovercss} WHERE [hid] = :hid", [':hid' => $hover_id])
      ->fetchAssoc();
  }

}
