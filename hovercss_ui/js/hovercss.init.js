/**
 * @file
 * Contains definition of the behaviour Hover.css.
 */

(function ($, Drupal, drupalSettings, once) {
  "use strict";

  // const compat = drupalSettings.hovercss.compat;

  Drupal.behaviors.hoverCSS = {
    attach: function (context, settings) {

      const elements = drupalSettings.hovercss.elements;
      $.each(elements, function (index, element) {
        let options = {
          selector: element.selector,
          effect: element.effect,
          delay: element.delay,
          duration: element.duration,
        };

        if (Array.isArray(options.selector)) {
          $.each(options.selector, function (index, selector) {
            if (once('hovercss', selector).length) {
              options.selector = selector;
              new Drupal.hoverCSS(options);
            }
          });
        }
        else {
          if (once('hovercss', options.selector).length) {
            new Drupal.hoverCSS(options);
          }
        }
      });

    }
  };

  Drupal.hoverCSS = function (options) {
    // Build Hover.css class from global HoverCSS settings.
    if (options.effect) {
      let Class = `hvr-${options.effect}`;

      // Add Hover.css custom properties.
      $(options.selector).removeAttr('style');
      if (options.delay && options.delay !== '0') {
        $(options.selector).css({
          '-webkit-transition-delay': options.delay + 'ms',
          '-moz-transition-delay': options.delay + 'ms',
          '-ms-transition-delay': options.delay + 'ms',
          '-o-transition-delay': options.delay + 'ms',
          'transition-delay': options.delay + 'ms',
        });
      }
      if (options.duration && options.duration !== '0') {
        $(options.selector).css({
          '-webkit-transition-duration': options.duration + 'ms',
          '-moz-transition-duration': options.duration + 'ms',
          '-ms-transition-duration': options.duration + 'ms',
          '-o-transition-duration': options.duration + 'ms',
          'transition-duration': options.duration + 'ms',
        });
      }

      // Add Hover.css classes.
      $(options.selector).addClass(Class);
    }
  };

})(jQuery, Drupal, drupalSettings, once);
