/**
 * @file
 * Contains definition of the behaviour Hover.css.
 */

(function ($, Drupal, drupalSettings, once) {
  "use strict";

  Drupal.behaviors.hoverCSS = {
    attach: function (context, settings) {

      const example = drupalSettings.hovercss.sample;
      const Selector = example.selector;

      if (once('hover__sample', Selector).length) {
        let options = {
          selector: Selector,
          effect: example.effect,
          delay: example.delay,
          duration: example.duration,
        };

        // Hover.css preview replay.
        $(once('hover-effect', '#edit-effect', context)).on(
          'change',
          function (event) {
            let $button = $(Selector);
            let classList = $button.attr('class').split(' ');

            $.each(classList, function (id, item) {
              if (item.indexOf('hvr-') == 0) {
                $button.removeClass(item);
              }
            });

            let options = {
              selector: Selector,
              effect: $('#edit-effect').val(),
              delay: $('#edit-delay').val(),
              duration: $('#edit-duration').val(),
            };

            setTimeout(function () {
              new Drupal.hoverCSSdemo(options);
            }, 10);

            event.preventDefault();
          }
        ).trigger('change');;
      }
    }
  };

  Drupal.hoverCSSdemo = function (options) {
    // Build Hover.css class from global HoverCSS settings.
    if (options.effect) {
      let Class = `hvr-${options.effect}`;

      // Add Hover.css classes.
      $(options.selector).addClass(Class);

      // Add Hover.css custom properties.
      $(options.selector).removeAttr('style');
      if (options.delay && options.delay !== '0') {
        $(options.selector).css({
          '-webkit-transition-delay': options.delay + 'ms',
          '-moz-transition-delay': options.delay + 'ms',
          '-ms-transition-delay': options.delay + 'ms',
          '-o-transition-delay': options.delay + 'ms',
          'transition-delay': options.delay + 'ms',
        });
      }
      if (options.duration && options.duration !== '0') {
        $(options.selector).css({
          '-webkit-transition-duration': options.duration + 'ms',
          '-moz-transition-duration': options.duration + 'ms',
          '-ms-transition-duration': options.duration + 'ms',
          '-o-transition-duration': options.duration + 'ms',
          'transition-duration': options.duration + 'ms',
        });
      }
    }
  };

})(jQuery, Drupal, drupalSettings, once);
